package com.farmcrowdy.farmcrowdyfarmers.messageTFS;

public interface MessageTFSPresenter {
    void sendMessage(String message, String photoLink, String videoLink, String audioLink);
}
