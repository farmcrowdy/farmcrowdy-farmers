package com.farmcrowdy.farmcrowdyfarmers.messageTFS;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdyfarmers.R;
import com.farmcrowdy.farmcrowdyfarmers.engines.LoadEffecient;
import com.farmcrowdy.farmcrowdyfarmers.silicompressorr.SiliCompressor;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;

import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MessageTfsActivity extends AppCompatActivity implements View.OnClickListener, MessageTFSInterface{

    private static final int SELECT_VIDEO = 4;
    private static final int RECORD_REQUEST_CODE = 401;
    static final int REQUEST_VIDEO_CAPTURE = 1;
    String imageLink="";
    String videoPath = "";
    String audioPath = "";
    TextView videoPick, imagePick, audioPick;
    TextView mb;
    EditText editText;
    ProgressDialog progressDialog;
    MessageTFSPresenter presenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_tfs);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Compressing video");

        editText = findViewById(R.id.message_content_id);

        videoPick = findViewById(R.id.videoClickId);
        audioPick = findViewById(R.id.audioClick);
        imagePick = findViewById(R.id.imageClickId);
        mb = findViewById(R.id.cmd);

        presenter = new MessageTFSPresenterImpl(this);

        imagePick.setOnClickListener(this);
        audioPick.setOnClickListener(this);
        videoPick.setOnClickListener(this);

        CardView bottomCard = findViewById(R.id.card_bottom);

                bottomCard.setVisibility(View.VISIBLE);
                bottomCard.setAnimation(AnimationUtils.loadAnimation(MessageTfsActivity.this, R.anim.slider_in_bottom));


        findViewById(R.id.sendId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(editText.getText()) && imageLink.isEmpty() && videoPath.isEmpty() && audioPath.isEmpty()) {
                    Snackbar.make(getCurrentFocus(), "Please create a message first", Snackbar.LENGTH_SHORT).show();
                }
                else {

                    if(!TextUtils.isEmpty(videoPath)) {
                        progressDialog.show();
                        File f = new File(Environment.getExternalStorageDirectory()+  "/media/videos");
                        if(f.mkdirs() || f.isDirectory()) {
                            new VideoCompressAsyncTask(MessageTfsActivity.this).execute(videoPath, f.getPath());
                        }
                        else Toast.makeText(MessageTfsActivity.this, "DEMN IT: still ant find environment", Toast.LENGTH_SHORT).show();

                    }
                    else {
                        progressDialog.setMessage("Sending message");
                        progressDialog.show();
                        presenter.sendMessage(String.valueOf(editText.getText()), imageLink,videoPath, audioPath);
                    }
                }
            }
        });

    }


    void showVideoStuffDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        //builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Select one:-");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Start a new video recording");
        arrayAdapter.add("Select video from gallery");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0: startForRecording();
                        break;
                    case 1:chooseVideoFromGallery();
                        break;
                }
            }
        });

        builderSingle.show();
    }
    private void captureImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }


    void startForRecording() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            RECORD_REQUEST_CODE);
                }
                else startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
            }
            else startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
        else Toast.makeText(this, "This phone seems not have a video recording capability", Toast.LENGTH_SHORT).show();
    }
    void chooseVideoFromGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_VIDEO);
    }

    @Override
    public void isSuccessful() {
        progressDialog.dismiss();
        progressDialog.cancel();
        Toast.makeText(this, "Message sent successfully", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void justFailed(String message) {
        progressDialog.dismiss();
        progressDialog.cancel();
        AlertDialog.Builder alerter = new AlertDialog.Builder(this);
        alerter.setMessage("Error due to:" + message +". Please try again");
        alerter.setTitle("Failed");
        alerter.show();
    }

    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;

        public VideoCompressAsyncTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... paths) {
            String filePath = null;
            try {

                filePath = SiliCompressor.with(mContext).compressVideo(Uri.parse(paths[0]), paths[1]);

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return filePath;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
            progressDialog.setMessage("Video compression completed, uploading data");
            File imageFile = new File(compressedFilePath);
            float length = imageFile.length() / 1024f; // Size in KB
            String value;
            if (length >= 1024)
                value = length / 1024f + " MB";
            else
                value = length + " KB";
            presenter.sendMessage(String.valueOf(editText.getText()), imageLink,compressedFilePath, audioPath);


        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:

                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    imageLink = resultUri.getPath();
                    if (!imageLink.equals("empty")) {
                        Bitmap high;
                        try {
                            high = new LoadEffecient().decodeSampledBitmapFromFile(imageLink, LoadEffecient.maxHeight, LoadEffecient.maxWidth);
                            File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                           // imagePick.setImageURI(Uri.parse(file.getPath()));
                            mb.append("\n Photo selected");

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
                break;

            case REQUEST_VIDEO_CAPTURE:
                if (resultCode == RESULT_OK) {
                    videoPath = data.getData().toString();
                    String sPath = getPath(data.getData());
                    String filename = sPath.substring(sPath.lastIndexOf("/") + 1);

                    mb.append("\n video selected:" + filename);
                } else {
                    Toast.makeText(this, "Video cancelled", Toast.LENGTH_SHORT).show();
                }
                break;

            case SELECT_VIDEO:
                if (getPath(data.getData()) == null)
                    Toast.makeText(this, "Couldn't find the video path, please try again", Toast.LENGTH_SHORT).show();
                else {
                    videoPath = data.getData().toString();
                    String sPath = getPath(data.getData());
                    File imageFile = new File(sPath);
                    String filename = sPath.substring(sPath.lastIndexOf("/") + 1);

                    mb.append("\n video selected:" + filename);
                }
                break;

            case  890: {
                if (resultCode == RESULT_OK) {
                    // Great! User has recorded and saved the audio file
                    audioPath = Environment.getExternalStorageDirectory() + "/recorded_fcy_audio.wav";
                    File imageFile = new File(audioPath);
                    float length = imageFile.length() / 1024f; // Size in KB
                    String value;
                    if (length >= 1024)
                        value = length / 1024f + " MB";
                    else
                        value = length + " KB";

                    mb.append("\n Recording done with: " + value);

                } else if (resultCode == RESULT_CANCELED) {
                    // Oops! User has canceled the recording
                }
            }
            break;
        }
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }


    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == RECORD_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();

            } else {

                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();

            }

        }
        else if (requestCode == 870) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                String filePath = Environment.getExternalStorageDirectory() + "/recorded_fcy_audio.wav";
                int color = getResources().getColor(R.color.colorPrimary);
                int requestCoder = 890;
                AndroidAudioRecorder.with(this)
                        // Required
                        .setFilePath(filePath)
                        .setColor(color)
                        .setRequestCode(requestCoder)

                        // Optional
                        .setSource(AudioSource.MIC)
                        .setChannel(AudioChannel.STEREO)
                        .setSampleRate(AudioSampleRate.HZ_11025)
                        .setAutoStart(false)
                        .setKeepDisplayOn(true)

                        // Start recording
                        .record();
                Toast.makeText(this, "record permission granted", Toast.LENGTH_LONG).show();

            } else {

                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();

            }


        }
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.videoClickId:
                showVideoStuffDialog();
                break;
            case R.id.imageClickId:
                captureImage();
                break;
            case R.id.audioClick:
                String filePath = Environment.getExternalStorageDirectory() + "/recorded_fcy_audio.wav";
                int color = getResources().getColor(R.color.colorPrimary);
                int requestCode = 890;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.RECORD_AUDIO)
                            != PackageManager.PERMISSION_GRANTED ) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO },
                                870);
                    } else {
                        AndroidAudioRecorder.with(this)
                                // Required
                                .setFilePath(filePath)
                                .setColor(color)
                                .setRequestCode(requestCode)

                                // Optional
                                .setSource(AudioSource.MIC)
                                .setChannel(AudioChannel.STEREO)
                                .setSampleRate(AudioSampleRate.HZ_11025)
                                .setAutoStart(false)
                                .setKeepDisplayOn(true)

                                // Start recording
                                .record();
                    }
                    break;
                }


        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
