package com.farmcrowdy.farmcrowdyfarmers.messageTFS;

public interface MessageTFSInterface {
    void isSuccessful();
    void justFailed(String message);
}
