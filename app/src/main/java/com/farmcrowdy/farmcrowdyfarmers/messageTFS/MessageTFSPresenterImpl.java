package com.farmcrowdy.farmcrowdyfarmers.messageTFS;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.farmcrowdy.farmcrowdyfarmers.ApplicationInstance;
import com.farmcrowdy.farmcrowdyfarmers.LoginActivity;
import com.farmcrowdy.farmcrowdyfarmers.MyEndpoint;
import com.farmcrowdy.farmcrowdyfarmers.engines.LoadEffecient;
import com.farmcrowdy.farmcrowdyfarmers.engines.TinyDB;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Multipart;

public class MessageTFSPresenterImpl implements MessageTFSPresenter {
    MessageTFSInterface messageTFSInterface;

    String audioName="empty", videoName="empty", photoName="empty";
    RequestBody audioNameBody, videoNameBody, photoNameBody;

    public MessageTFSPresenterImpl(MessageTFSInterface messageTFSInterface) {
        this.messageTFSInterface = messageTFSInterface;
    }

    private MultipartBody.Part getVideoPart(String filePath) {

        File file = new File(filePath);
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("video/*"), file
                );
        videoName = "ChatVideo"+new Date().getTime()+".mp4";
        this.videoNameBody = RequestBody.create(MediaType.parse("text/plain"), videoName);

        return MultipartBody.Part.createFormData("messagevideo", videoName, requestFile);
    }

    private MultipartBody.Part getAudioPart(String filePath) {

        File file = new File(filePath);
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("audio/*"), file
                );
        audioName = "ChatAudio"+new Date().getTime()+".wav";
        this.audioNameBody = RequestBody.create(MediaType.parse("text/plain"), audioName);

        return MultipartBody.Part.createFormData("messageaudio", audioName, requestFile);
    }

    private MultipartBody.Part getPhotoPart(String filePath) {
        Bitmap high = null;
        try {
            high = new LoadEffecient().decodeSampledBitmapFromFile(filePath, LoadEffecient.maxHeight, LoadEffecient.maxWidth);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Bitmap low = new LoadEffecient().decodeSampledBitmapFromFile(filePath, 80.0f, 80.0f);
        File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("*/*"), file
                );
        photoName = "chatPhoto"+new Date().getTime()+".jpg";
        this.photoNameBody = RequestBody.create(MediaType.parse("text/plain"), photoName);

        return MultipartBody.Part.createFormData("m_image", photoName, requestFile);
    }

    @Override
    public void sendMessage(String message, String photoLink, String videoLink, String audioLink) {
        MultipartBody.Part photoBody = null;
        MultipartBody.Part videoBody  = null;
        MultipartBody.Part audioBody = null;
        if(!TextUtils.isEmpty(videoLink)) {
            videoBody = getVideoPart(videoLink);
        }
        if(!TextUtils.isEmpty(photoLink)){
            photoBody = getPhotoPart(photoLink);
        }
        if(!TextUtils.isEmpty(audioLink)) {
            audioBody = getAudioPart(audioLink);
        }

        int farmer_id = new TinyDB(ApplicationInstance.getContext()).getInt("login", 1);

        RequestBody pRequest  =  RequestBody.create(MediaType.parse("text/plain"), photoName);
        RequestBody vRequest = RequestBody.create(MediaType.parse("text/plain"), videoName);
        RequestBody aRequest = RequestBody.create(MediaType.parse("text/plain"), audioName);
        RequestBody messageBody = RequestBody.create(MediaType.parse("text/plain"), message);

        MyEndpoint myEndpoint = ApplicationInstance.getRetrofitTfs().create(MyEndpoint.class);

        Call<ResponseBody> caller = myEndpoint.sendMessageToTFS(farmer_id, 2,farmer_id,messageBody,vRequest, pRequest, aRequest,videoBody,photoBody,audioBody);
        caller.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) {
                messageTFSInterface.isSuccessful();
                }
                else messageTFSInterface.justFailed(response.message() + " while going");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            messageTFSInterface.justFailed(t.getMessage() + "from start");
            }
        });

    }
}
