package com.farmcrowdy.farmcrowdyfarmers;

public class PagaResponsePojo {
    int responseCode;
    String message,refernceNumber, errorMessage, transactionId;
    int withdrawalCode;

    public String getTransactionId() {
        return transactionId;
    }

    public int getWithdrawalCode() {
        return withdrawalCode;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getMessage() {
        return message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getRefernceNumber() {
        return refernceNumber;
    }
}
