package com.farmcrowdy.farmcrowdyfarmers.wallets;

public class PagaAirtimeBody {


    String referenceNumber,locale,destinationPhoneNumber, purchaserPrincipal, purchaserCredentials;
    double amount;

    public PagaAirtimeBody(String referenceNumber, String locale, String destinationPhoneNumber, String purchaserPrincipal, String purchaserCredentials, double amount) {
        this.referenceNumber = referenceNumber;
        this.locale = locale;
        this.destinationPhoneNumber = destinationPhoneNumber;
        this.purchaserPrincipal = purchaserPrincipal;
        this.purchaserCredentials = purchaserCredentials;
        this.amount = amount;
    }
}
