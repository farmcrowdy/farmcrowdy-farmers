package com.farmcrowdy.farmcrowdyfarmers.wallets;

public class PagaAccountBalanceBody {
   String referenceNumber,accountPrincipal, accountCredentials,locale;

    public PagaAccountBalanceBody(String referenceNumber, String accountPrincipal, String accountCredentials, String locale) {
        this.referenceNumber = referenceNumber;
        this.accountPrincipal = accountPrincipal;
        this.accountCredentials = accountCredentials;
        this.locale = locale;
    }
}
