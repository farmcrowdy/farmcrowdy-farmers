package com.farmcrowdy.farmcrowdyfarmers.wallets;

public class PagaAccountBalanceResponse {
    double availableBalance;
    int responseCode;
    String message;

    public double getAvailableBalance() {
        return availableBalance;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getMessage() {
        return message;
    }
}
