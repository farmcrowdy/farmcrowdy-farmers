package com.farmcrowdy.farmcrowdyfarmers.wallets;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdyfarmers.ApplicationInstance;
import com.farmcrowdy.farmcrowdyfarmers.MyEndpoint;
import com.farmcrowdy.farmcrowdyfarmers.PagaResponsePojo;
import com.farmcrowdy.farmcrowdyfarmers.R;
import com.farmcrowdy.farmcrowdyfarmers.engines.OfflineDb;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rafakob.floatingedittext.FloatingEditText;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class BankDepostActivity extends AppCompatActivity {
    FloatingEditText deposit_amount, deposit_phone, depost_accountNumber;
    ArrayList<String> bankUUIDList, banksNameList ;
    String accountOwner, chosenBankUUID;
    Spinner bankSpinner;

    ProgressDialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bank_deposts_layout);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        banksNameList = new ArrayList<>();
        bankUUIDList = new ArrayList<>();

        deposit_amount = findViewById(R.id.depost_amount);
        deposit_phone = findViewById(R.id.deposit_phone);
        depost_accountNumber = findViewById(R.id.deposit_number);
        bankSpinner = findViewById(R.id.bank_spinner);

        loadListOfBanksFromPaga();



        findViewById(R.id.depositButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!deposit_amount.getText().isEmpty()) {
                    if(TextUtils.getTrimmedLength(depost_accountNumber.getText()) ==10) {
                        if(TextUtils.getTrimmedLength(deposit_phone.getText()) == 11) {
                        //Do work
                            validateBankAccount();

                        }
                        else {
                           Snackbar.make(getCurrentFocus(), "Phone number provided has an incorrect format", Snackbar.LENGTH_SHORT).show();
                        }

                    }
                    else {
                        Snackbar.make(getCurrentFocus(), "Account number provided is incorrect", Snackbar.LENGTH_SHORT).show();
                    }
                }
                else {
                    Snackbar.make(getCurrentFocus(), "Please enter an amount of money", Snackbar.LENGTH_SHORT).show();
                }
            }
        });




    }

    void validateBankAccount() {
        progressDialog.setMessage("Checking bank account...");
        progressDialog.show();
        Observable<String> validateObs = Observable.create(subscriber -> {
            OfflineDb offlineDb = new OfflineDb(BankDepostActivity.this);
            try {

                String referenceNumber = generateRandomReferenceNumber(offlineDb.getFirstname());
                String customerPrincipal = sortPhoneNumberExtension(offlineDb.getPhone());
                String customerCredentials = "Farmcrowdy" + offlineDb.getPin();
                String destinationPhoneNumber = sortPhoneNumberExtension(deposit_phone.getText());

                Log.d("HASH", "has is : " + chosenBankUUID);

                String sBuilder = referenceNumber + ""+String.valueOf(deposit_amount.getText()) +""+ String.valueOf(chosenBankUUID)+""+ String.valueOf(depost_accountNumber.getText()) + ""+ ApplicationInstance.hashKey;

                Log.d("HASH", "has is : " + sBuilder);

                final String hmac = hashSHA512(sBuilder).toString();

                Log.d("HASH", "has is : " + hmac);
                Gson gson = new GsonBuilder()
                        .setLenient()
                        .create();

                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                httpClient.addInterceptor(chain -> {
                    Request original = chain.request();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("principal", ApplicationInstance.pagaPrincipal)
                            .addHeader("credentials", ApplicationInstance.pagaCredentials)
                            .addHeader("hash", hmac);

                    Request request = requestBuilder.build();

                    return chain.proceed(request);
                });

                OkHttpClient client = httpClient.build();
                Retrofit retrofit = new Retrofit.Builder()
                        .client(client)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .baseUrl(ApplicationInstance.pagaBaseUrl)
                        .build();


                MyEndpoint myEndpoint = retrofit.create(MyEndpoint.class);
                // Call<ListOfBanksPojo>  caller = myEndpoint.buyAirtimePaga(new PagaBuyAirtime(referenceNumber, "en"));
                Call<ValidateBankPojo> caller = myEndpoint.validateAccount(new PagaValidaateBody(referenceNumber, deposit_phone.getText(), chosenBankUUID,depost_accountNumber.getText(), Double.valueOf(deposit_amount.getText())));
                caller.enqueue(new Callback<ValidateBankPojo>() {
                    @Override
                    public void onResponse(Call<ValidateBankPojo> call, Response<ValidateBankPojo> response) {
                        Log.e("Pagatech", "error exception due to inner " + response.message());

                        if(response.isSuccessful()) {
                            accountOwner = response.body().getDestinationAccountHolderNameAtBank();
                                subscriber.onNext(response.body().getDestinationAccountHolderNameAtBank());
                                subscriber.onCompleted();
                        }
                        else {
                            subscriber.onNext("nullpointer: "+response.message());
                            subscriber.onCompleted();
                        }
                        //withPhotoInterface.failure("error due to: "+ response.code());
                    }

                    @Override
                    public void onFailure(Call<ValidateBankPojo> call, Throwable t) {
                        subscriber.onNext("nullpointer: "+t.getMessage());
                        subscriber.onCompleted();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onNext("nullpointer: "+e.getMessage());
                subscriber.onCompleted();
                Log.e("Pagatech", "error exception due to at catch " + e.getMessage());
            }
        });

        validateObs.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                progressDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(BankDepostActivity.this);
                builder.setCancelable(false);
                if(s.contains("nullpointer")) {
                    builder.setMessage("The account number provided is not valid. Please check and try again");
                    builder.setCancelable(true);
                    builder.setTitle("Invalid");
                    builder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                       dialogInterface.dismiss();
                       dialogInterface.cancel();
                        }
                    });
                }
                else {
                    builder.setTitle("Confirm");
                    builder.setMessage("Are you sure you want to send "+ deposit_amount.getText() +" to the account holder "+depost_accountNumber.getText() +" ("+s+") ?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        caller();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        dialogInterface.cancel();
                        }
                    });
                }
                builder.show();
            }
        });

    }


    void caller() {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + getUSSD()));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(BankDepostActivity.this, new String[]{Manifest.permission.CALL_PHONE},407);
                return;

            }
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("Calling a Phone Number", "Call failed", activityException);
        }
    }
    String getUSSD() {
        return "*242*7*" +
                deposit_amount.getText() +
                "*" +
                depost_accountNumber.getText() +
                Uri.encode("#");
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 407: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + getUSSD()));
                    startActivity(callIntent);
                }
                else
                {

                }
                return;
            }
        }
    }



    void doFinallyDeposit(String accountName) {
        progressDialog.setMessage("Sending "+deposit_amount.getText() + " to "+accountName);
        progressDialog.show();
        Observable<String> sendObs = Observable.create(subscriber -> {
            OfflineDb offlineDb = new OfflineDb(BankDepostActivity.this);
            try {
                String referenceNumber = generateRandomReferenceNumber(offlineDb.getFirstname());
                String customerPrincipal = sortPhoneNumberExtension(offlineDb.getPhone());
                String customerCredentials = "Farmcrowdy" + offlineDb.getPin();
                String destinationPhoneNumber = sortPhoneNumberExtension(deposit_phone.getText());


                String sBuilder = referenceNumber + String.valueOf(deposit_amount.getText()) + chosenBankUUID+ depost_accountNumber.getText() +  ApplicationInstance.hashKey;

                Log.d("HASH", "has is : " + sBuilder);


                final String hmac = hashSHA512(sBuilder).toString();
                Log.d("HASH", "has is : " + hmac);
                Gson gson = new GsonBuilder()
                        .setLenient()
                        .create();

                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                httpClient.addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        // Request customization: add request headers
                        Request.Builder requestBuilder = original.newBuilder()
                                .addHeader("Content-Type", "application/json")
                                .addHeader("principal", ApplicationInstance.pagaPrincipal)
                                .addHeader("credentials", ApplicationInstance.pagaCredentials)
                                .addHeader("hash", hmac);

                        Request request = requestBuilder.build();

                        return chain.proceed(request);
                    }
                });

                OkHttpClient client = httpClient.build();
                Retrofit retrofit = new Retrofit.Builder()
                        .client(client)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .baseUrl(ApplicationInstance.pagaBaseUrl)
                        .build();


                MyEndpoint myEndpoint = retrofit.create(MyEndpoint.class);
                // Call<ListOfBanksPojo>  caller = myEndpoint.buyAirtimePaga(new PagaBuyAirtime(referenceNumber, "en"));
                Call<PagaResponsePojo> caller = myEndpoint.finalDepositToAccount(new PagaValidaateBody(referenceNumber,customerPrincipal,customerCredentials,"",chosenBankUUID,depost_accountNumber.getText(),Double.valueOf(deposit_amount.getText())));
                caller.enqueue(new Callback<PagaResponsePojo>() {
                    @Override
                    public void onResponse(Call<PagaResponsePojo> call, Response<PagaResponsePojo> response) {
                        if(response.isSuccessful()) {
                            if(response.body().getResponseCode() == 0) {
                                subscriber.onNext("Yes");
                                subscriber.onCompleted();
                            }
                           else {
                                subscriber.onNext(response.body().getMessage());
                                subscriber.onCompleted();
                            }
                        }
                        else {
                            subscriber.onNext("Error "+response.message());
                            subscriber.onCompleted();
                        }
                        //withPhotoInterface.failure("error due to: "+ response.code());
                    }

                    @Override
                    public void onFailure(Call<PagaResponsePojo> call, Throwable t) {
                        subscriber.onNext("Error "+t.getMessage());
                        subscriber.onCompleted();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onNext("Error "+e.getMessage());
                subscriber.onCompleted();
                Log.e("Pagatech", "error exception due to " + e.getMessage());
            }
        });
        sendObs.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                progressDialog.dismiss();
                progressDialog.cancel();
                showResult(s);
            }
        });
    }

    void showResult(String result) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if(result.equals("Yes")) {
            builder.setTitle("Successful");
            builder.setMessage("Money sent to "+accountOwner +" successfully. A log of this transaction can be seen in your transaction history.");
            builder.setCancelable(false);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });

        }
        else {
            builder.setTitle("Failed");
            builder.setMessage("Some thing went while trasffering money due to: "+result);
            builder.setCancelable(true);
            builder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    dialogInterface.cancel();
                }
            });
        }
        builder.show();

    }





    void loadListOfBanksFromPaga() {
        progressDialog.setMessage("Loading available banks, please wait...");
        progressDialog.show();
        Observable<ArrayList<MainBankListPojo>> bankListObs = Observable.create(subscriber -> {
            OfflineDb offlineDb = new OfflineDb(BankDepostActivity.this);
                String referenceNumber = generateRandomReferenceNumber(offlineDb.getFirstname());
                String customerPrincipal = sortPhoneNumberExtension(offlineDb.getPhone());
                String customerCredentials = "Farmcrowdy"+offlineDb.getPin();
                String destinationPhoneNumber = sortPhoneNumberExtension(deposit_phone.getText());


                String sBuilder = referenceNumber+ApplicationInstance.hashKey;
                Log.d("HASH", "has is : "+ sBuilder);


                final String hmac = hashSHA512(sBuilder).toString();
                Log.d("HASH", "has is : "+ hmac);
                Gson gson = new GsonBuilder()
                        .setLenient()
                        .create();

                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                httpClient.addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        // Request customization: add request headers
                        Request.Builder requestBuilder = original.newBuilder()
                                .addHeader("Content-Type", "application/json")
                                .addHeader("principal", ApplicationInstance.pagaPrincipal)
                                .addHeader("credentials",ApplicationInstance.pagaCredentials)
                                .addHeader("hash", hmac);

                        Request request = requestBuilder.build();

                        return chain.proceed(request);
                    }
                });

                OkHttpClient client = httpClient.build();
                Retrofit retrofit = new Retrofit.Builder()
                        .client(client)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .baseUrl(ApplicationInstance.pagaBaseUrl)
                        .build();



                MyEndpoint myEndpoint = retrofit.create(MyEndpoint.class);
                // Call<ListOfBanksPojo>  caller = myEndpoint.buyAirtimePaga(new PagaBuyAirtime(referenceNumber, "en"));
                Call<ListOfBanksPojo> caller = myEndpoint.listOfBanksPaga(new PagaBankListBody(referenceNumber,"en"));
                caller.enqueue(new Callback<ListOfBanksPojo>() {
                    @Override
                    public void onResponse(Call<ListOfBanksPojo> call, Response<ListOfBanksPojo> response) {
                        if(response.isSuccessful()) {

                            if(response.body().getResponseCode() == 0) {
                                subscriber.onNext(response.body().getBanks());
                                subscriber.onCompleted();
                            }
                            else {
                                subscriber.onNext(null);
                                subscriber.onCompleted();
                            }
                        }

                        else {
                            subscriber.onNext(null);
                            subscriber.onCompleted();
                        }
                        //withPhotoInterface.failure("error due to: "+ response.code());
                    }

                    @Override
                    public void onFailure(Call<ListOfBanksPojo> call, Throwable t) {
                        Log.e("Pagatech", "error exception due to "+t.getMessage());
                        subscriber.onNext(null);
                        subscriber.onCompleted();
                    }
                });

        });

        bankListObs.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(mainBankLists -> {
            progressDialog.dismiss();
            progressDialog.cancel();
            if(mainBankLists !=null) {
            for(int i=0; i<mainBankLists.size(); i++) {
                banksNameList.add(mainBankLists.get(i).getName());
                bankUUIDList.add(mainBankLists.get(i).getUuid());

                ArrayAdapter<String> dataAdapteFarmGroup = new ArrayAdapter<String>(BankDepostActivity.this,
                        android.R.layout.simple_spinner_item, banksNameList);
                dataAdapteFarmGroup.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                bankSpinner.setAdapter(dataAdapteFarmGroup);

                progressDialog.dismiss();
                progressDialog.cancel();

                bankSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        chosenBankUUID = bankUUIDList.get(i);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            }
            }
            else {
                Toast.makeText(BankDepostActivity.this, "Banks could not be loaded", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    public static Object hashSHA512(String message) {

        try {
            String toReturn = null;
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-512");
                digest.reset();
                digest.update(message.getBytes("utf8"));
                toReturn = String.format("%040x", new BigInteger(1, digest.digest()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return toReturn;

        } catch(Exception e) {
            return null;
        }
    }

    String sortPhoneNumberExtension(String initialNumber) {
        initialNumber = "08137102286";
        String finalString = "";
        if(initialNumber.substring(0,1).equals("0") ) {
            finalString = "+234"+initialNumber.substring(1);
        }
        return  finalString;
    }
    String generateRandomReferenceNumber(String firstname) {
        String initial = "Farmcrowdy";
        int middle = new Random().nextInt(9999);
        String lastOne = firstname.substring(2);
        long timing = new Date().getTime();
        return String.valueOf(initial+middle+""+timing);
    }
}
