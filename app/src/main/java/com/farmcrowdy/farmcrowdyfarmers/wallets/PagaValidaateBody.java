package com.farmcrowdy.farmcrowdyfarmers.wallets;

public class PagaValidaateBody {
    String referenceNumber,recipientPhoneNumber, destinationBankUUID, destinationBankAccountNumber, recipientName;
    double amount;

    public PagaValidaateBody(String referenceNumber, String recipientPhoneNumber, String destinationBankUUID, String destinationBankAccountNumber,  double amount) {
        this.referenceNumber = referenceNumber;
        this.recipientPhoneNumber = recipientPhoneNumber;
        this.destinationBankUUID = destinationBankUUID;
        this.destinationBankAccountNumber = destinationBankAccountNumber;
        this.amount = amount;
    }

    String accountPrincipal, accountCredentials, transferReference, destinationBank, destinationAccount, agentId ;

    public PagaValidaateBody(String referenceNumber, String accountPrincipal, String accountCredentials, String transferReference, String destinationBank, String destinationAccount, double amount) {
        this.referenceNumber = referenceNumber;
        this.accountPrincipal = accountPrincipal;
        this.accountCredentials = accountCredentials;
        this.transferReference = transferReference;
        this.destinationBank = destinationBank;
        this.destinationAccount = destinationAccount;
        this.amount = amount;
    }


    public PagaValidaateBody(String referenceNumber, String accountPrincipal, String accountCredentials, String transferReference, String agentId, double amount) {
        this.referenceNumber = referenceNumber;
        this.accountPrincipal = accountPrincipal;
        this.accountCredentials = accountCredentials;
        this.transferReference = transferReference;
        this.agentId = agentId;
        this.amount = amount;
    }
}
