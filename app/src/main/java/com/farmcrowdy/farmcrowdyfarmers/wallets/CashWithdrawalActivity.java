package com.farmcrowdy.farmcrowdyfarmers.wallets;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.farmcrowdy.farmcrowdyfarmers.ApplicationInstance;
import com.farmcrowdy.farmcrowdyfarmers.MyEndpoint;
import com.farmcrowdy.farmcrowdyfarmers.PagaResponsePojo;
import com.farmcrowdy.farmcrowdyfarmers.R;
import com.farmcrowdy.farmcrowdyfarmers.engines.OfflineDb;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rafakob.floatingedittext.FloatingEditText;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Date;
import java.util.Random;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.Observable;

public class CashWithdrawalActivity extends AppCompatActivity {
    FloatingEditText amount, agentId;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cash_withdrawal_layout);
        amount = findViewById(R.id.cash_amount);
        agentId = findViewById(R.id.cash_agent_id);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        findViewById(R.id.withdrawButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!amount.getText().isEmpty()) {
                    if(!agentId.getText().isEmpty()) {
                        doCashOut();
                    }
                    else {
                        Snackbar.make(getCurrentFocus(), "Enter a correct agent Id", Snackbar.LENGTH_SHORT).show();
                    }
                }
                else {
                    Snackbar.make(getCurrentFocus(), "Enter an amount you want to withdraw", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    void doCashOut() {
        progressDialog.setMessage("Withdrawing "+amount.getText()+" from agent with ID "+agentId);
        progressDialog.show();
        Observable<String> sendObs = Observable.create(subscriber -> {
            OfflineDb offlineDb = new OfflineDb(CashWithdrawalActivity.this);
            try {
                String referenceNumber = generateRandomReferenceNumber(offlineDb.getFirstname());
                String customerPrincipal = sortPhoneNumberExtension(offlineDb.getPhone());
                String customerCredentials = "Farmcrowdy" + offlineDb.getPin();
               // String destinationPhoneNumber = sortPhoneNumberExtension(deposit_phone.getText());

                String sBuilder = referenceNumber + String.valueOf(amount.getText()) + agentId +  ApplicationInstance.hashKey;

                Log.d("HASH", "has is : " + sBuilder);


                final String hmac = hashSHA512(sBuilder).toString();
                Log.d("HASH", "has is : " + hmac);
                Gson gson = new GsonBuilder()
                        .setLenient()
                        .create();

                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                httpClient.addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        // Request customization: add request headers
                        Request.Builder requestBuilder = original.newBuilder()
                                .addHeader("Content-Type", "application/json")
                                .addHeader("principal", ApplicationInstance.pagaPrincipal)
                                .addHeader("credentials", ApplicationInstance.pagaCredentials)
                                .addHeader("hash", hmac);

                        Request request = requestBuilder.build();

                        return chain.proceed(request);
                    }
                });

                OkHttpClient client = httpClient.build();
                Retrofit retrofit = new Retrofit.Builder()
                        .client(client)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .baseUrl(ApplicationInstance.pagaBaseUrl)
                        .build();


                MyEndpoint myEndpoint = retrofit.create(MyEndpoint.class);
                // Call<ListOfBanksPojo>  caller = myEndpoint.buyAirtimePaga(new PagaBuyAirtime(referenceNumber, "en"));
                Call<PagaResponsePojo> caller = myEndpoint.finalDepositToAccount(new PagaValidaateBody(referenceNumber,customerPrincipal,customerCredentials,"","","",0));
                caller.enqueue(new Callback<PagaResponsePojo>() {
                    @Override
                    public void onResponse(Call<PagaResponsePojo> call, Response<PagaResponsePojo> response) {
                        if(response.isSuccessful()) {
                            if(response.body().getResponseCode() == 0) {
                                subscriber.onNext("Yes");
                                subscriber.onCompleted();
                            }
                            else {
                                subscriber.onNext(response.body().getMessage());
                                subscriber.onCompleted();
                            }
                        }
                        else {
                            subscriber.onNext("Error "+response.message());
                            subscriber.onCompleted();
                        }
                        //withPhotoInterface.failure("error due to: "+ response.code());
                    }

                    @Override
                    public void onFailure(Call<PagaResponsePojo> call, Throwable t) {
                        subscriber.onNext("Error "+t.getMessage());
                        subscriber.onCompleted();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onNext("Error "+e.getMessage());
                subscriber.onCompleted();
                Log.e("Pagatech", "error exception due to " + e.getMessage());
            }
        });
    }


    public static Object hashSHA512(String message) {

        try {
            String toReturn = null;
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-512");
                digest.reset();
                digest.update(message.getBytes("utf8"));
                toReturn = String.format("%040x", new BigInteger(1, digest.digest()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return toReturn;

        } catch(Exception e) {
            return null;
        }
    }

    String sortPhoneNumberExtension(String initialNumber) {
        String finalString = "";
        if(initialNumber.substring(0,1).equals("0") ) {
            finalString = "+234"+initialNumber.substring(1);
        }
        return  finalString;
    }
    String generateRandomReferenceNumber(String firstname) {
        String initial = "Farmcrowdy";
        int middle = new Random().nextInt(9999);
        String lastOne = firstname.substring(2);
        long timing = new Date().getTime();
        return String.valueOf(initial+middle+""+timing);
    }
}
