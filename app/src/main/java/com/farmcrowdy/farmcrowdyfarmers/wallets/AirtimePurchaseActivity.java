package com.farmcrowdy.farmcrowdyfarmers.wallets;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.farmcrowdy.farmcrowdyfarmers.ApplicationInstance;
import com.farmcrowdy.farmcrowdyfarmers.MyEndpoint;
import com.farmcrowdy.farmcrowdyfarmers.PagaResponsePojo;
import com.farmcrowdy.farmcrowdyfarmers.R;
import com.farmcrowdy.farmcrowdyfarmers.engines.InternetCheck;
import com.farmcrowdy.farmcrowdyfarmers.engines.OfflineDb;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rafakob.floatingedittext.FloatingEditText;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Date;
import java.util.Observable;
import java.util.Random;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class AirtimePurchaseActivity extends AppCompatActivity {

    FloatingEditText amount, phone_number;
    ProgressDialog progressDialog;
    Button buyButton;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.airtime_layout);
        amount = findViewById(R.id.airtime_amount);
        phone_number = findViewById(R.id.airtime_number);
        buyButton = findViewById(R.id.buyAirtimeButton);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!amount.getText().isEmpty()) {
                    if(TextUtils.getTrimmedLength(phone_number.getText()) == 11) {
                        showQuestionOnData();
                    }
                    else {
                        Snackbar.make(getCurrentFocus(), "Phone number incorrect, use template 080XXXXXXXX",Snackbar.LENGTH_SHORT).show();
                    }
                }
                else {
                    Snackbar.make(getCurrentFocus(), "Please enter amount of airtime", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

    }
    void showChooser() {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setIcon(R.drawable.tfs_logo_);
        builderSingle.setTitle("Select method:-");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("offline: USSD");
        arrayAdapter.add("online: DATA");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: useUSSD();
                        break;
                    case 1:
                        new InternetCheck(internet-> {
                            if(internet)   useInternet();
                            else {
                                Snackbar.make(getCurrentFocus(), "No internet connection", Snackbar.LENGTH_SHORT).show();
                            }
                        });

                        break;
                }
            }
        });
        builderSingle.show();

    }
    void showQuestionOnData() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm airtime purchase");
        builder.setMessage("Are you sure you want to buy "+amount.getText() +" airtime for "+phone_number.getText() +"?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                dialogInterface.cancel();
                showChooser();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
           dialogInterface.dismiss();
           dialogInterface.cancel();
            }
        });
        builder.show();
    }
    void useInternet() {

        progressDialog.setMessage("Processing your airtime, please wait...");
    progressDialog.show();
        rx.Observable<String> myAirtimeRx = rx.Observable.create(new rx.Observable.OnSubscribe<String>() {
            @Override
            public void call(final Subscriber<? super String> subscriber) {
                OfflineDb offlineDb = new OfflineDb(AirtimePurchaseActivity.this);
                try {
                    String referenceNumber = generateRandomReferenceNumber(offlineDb.getFirstname());
                    String customerPrincipal = "+2349076890445";
                   // String customerCredentials = "Farmcrowdy"+offlineDb.getPin();
                    String customerCredentials = "Farmcrowdy5831";
                    String destinationPhoneNumber = "+2349076890445";


                    String sBuilder = referenceNumber+amount.getText()+destinationPhoneNumber+ApplicationInstance.hashKey;

                    Log.d("HASH", "has is : "+ sBuilder);


                    final String hmac = String.valueOf(hashSHA512(sBuilder));
                    Log.d("HASH", "has is : "+ hmac);
                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();

                    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                    httpClient.addInterceptor(new Interceptor() {
                        @Override
                        public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                            Request original = chain.request();

                            // Request customization: add request headers
                            Request.Builder requestBuilder = original.newBuilder()
                                    .addHeader("Content-Type", "application/json")
                                    .addHeader("principal", ApplicationInstance.pagaPrincipal)
                                    .addHeader("credentials", ApplicationInstance.pagaCredentials)
                                    .addHeader("hash", hmac);

                            Request request = requestBuilder.build();

                            return chain.proceed(request);
                        }
                    });

                    OkHttpClient client = httpClient.build();
                    Retrofit retrofit = new Retrofit.Builder()
                            .client(client)
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create(gson))
                            .baseUrl(ApplicationInstance.pagaBaseUrl)
                            .build();



                    MyEndpoint myEndpoint = retrofit.create(MyEndpoint.class);
                    // Call<PagaResponsePojo>  caller = myEndpoint.buyAirtimePaga(new PagaBuyAirtime(referenceNumber, "en"));
                    Call<PagaResponsePojo> caller = myEndpoint.buyAirtimeFromPaga(new PagaAirtimeBody(referenceNumber,"en",destinationPhoneNumber,customerPrincipal,customerCredentials,Double.valueOf(amount.getText())));
                    caller.enqueue(new Callback<PagaResponsePojo>() {
                        @Override
                        public void onResponse(Call<PagaResponsePojo> call, Response<PagaResponsePojo> response) {
                            Log.e("Pagatech", "Coder is  "+response.code());
                            if(response.isSuccessful()) {
                                if(response.body().getResponseCode() == -1) {
                                    subscriber.onNext("Yes");
                                    subscriber.onCompleted();
                                }
                                else {
                                    subscriber.onNext(response.body().getMessage());
                                    subscriber.onCompleted();
                                }
                            }

                           else {
                               subscriber.onNext("This is is too");
                               subscriber.onCompleted();
                           }
                            //withPhotoInterface.failure("error due to: "+ response.code());
                        }

                        @Override
                        public void onFailure(Call<PagaResponsePojo> call, Throwable t) {
                            subscriber.onNext(t.getMessage());
                            subscriber.onCompleted();
                        }
                    });
                }catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onNext(e.getMessage());
                    subscriber.onCompleted();
                    Log.e("Pagatech", "error exception due to "+e.getMessage());
                }
            }
        });

        myAirtimeRx.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                progressDialog.dismiss();
                progressDialog.cancel();
                showResult(s);

            }
        });
    }
    void useUSSD() {

    }

    public void showResult(String message) {
        String myMessage = message;
        String title = "Failed";
        String action = "Try again";
        switch (message) {
            case "Yes": myMessage = "Airtime purchase successful, you should receive your airtime in few seconds";
            title = "Successful";
            action = "Ok";
                break;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(myMessage);
        builder.setTitle(title);
        if(title.equals("Failed")) {
            builder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    dialogInterface.cancel();
                    useInternet();
                }
            });
        }
        else {
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    dialogInterface.cancel();
                    finish();
                }
            });
        }
        builder.show();

    }

    public static Object hashSHA512(String message) {

        try {
            String toReturn = null;
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-512");
                digest.reset();
                digest.update(message.getBytes("utf8"));
                toReturn = String.format("%040x", new BigInteger(1, digest.digest()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return toReturn;

        } catch(Exception e) {
            return null;
        }
    }

    String sortPhoneNumberExtension(String initialNumber) {
        String finalString = "";
        if(initialNumber.substring(0,1).equals("0") ) {
            finalString = "+234"+initialNumber.substring(1);
        }
        return  finalString;
    }
    String generateRandomReferenceNumber(String firstname) {
        String initial = "Farmcrowdy";
        int middle = new Random().nextInt(9999);
        String lastOne = firstname.substring(2);
        long timing = new Date().getTime();
        return String.valueOf(initial+middle+""+timing);
    }
}
