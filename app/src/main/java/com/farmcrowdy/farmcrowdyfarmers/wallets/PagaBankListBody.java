package com.farmcrowdy.farmcrowdyfarmers.wallets;

public class PagaBankListBody {
    String referenceNumber, locale;

    public PagaBankListBody(String referenceNumber, String locale) {
        this.referenceNumber = referenceNumber;
        this.locale = locale;
    }
}
