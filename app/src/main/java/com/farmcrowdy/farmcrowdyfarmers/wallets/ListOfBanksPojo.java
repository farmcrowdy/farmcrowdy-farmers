package com.farmcrowdy.farmcrowdyfarmers.wallets;

import java.util.ArrayList;

public class ListOfBanksPojo {
    ArrayList<MainBankListPojo> banks;
    int responseCode;

    public ArrayList<MainBankListPojo> getBanks() {
        return banks;
    }

    public int getResponseCode() {
        return responseCode;
    }
}
