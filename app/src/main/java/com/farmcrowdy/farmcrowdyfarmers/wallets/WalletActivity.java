package com.farmcrowdy.farmcrowdyfarmers.wallets;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdyfarmers.ApplicationInstance;
import com.farmcrowdy.farmcrowdyfarmers.MyEndpoint;
import com.farmcrowdy.farmcrowdyfarmers.R;
import com.farmcrowdy.farmcrowdyfarmers.engines.InternetCheck;
import com.farmcrowdy.farmcrowdyfarmers.engines.OfflineDb;
import com.farmcrowdy.farmcrowdyfarmers.engines.TinyDB;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WalletActivity extends AppCompatActivity {
    TextView balanceText;
    Button clickBalance;
    TextView myName, myPhone;
    CardView airtimeCard,bankCard, cashCard,electricCard, tvCard, moreCard;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        balanceText = findViewById(R.id.balanceText);
        clickBalance = findViewById(R.id.clickBalance);

        airtimeCard = findViewById(R.id.mobile_tpp);
        bankCard = findViewById(R.id.bankCard);
        cashCard = findViewById(R.id.cash_withdraw_card);
        electricCard = findViewById(R.id.payForElecticity);
        tvCard = findViewById(R.id.payForTv);

        myName = findViewById(R.id.myName);
        myPhone = findViewById(R.id.myPhone);
        moreCard = findViewById(R.id.payMoreBills);

              balanceText.setVisibility(View.GONE);
              clickBalance.setVisibility(View.VISIBLE);
              clickBalance.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                  seeBalanceUSSD();
                  }
              });
              myName.setText(new TinyDB(this).getString("name"));
              myPhone.setText(sortPhoneNumberExtension(new TinyDB(this).getString("farmer_phone")));


        CardView wholeCard = findViewById(R.id.whole_card);
        wholeCard.setVisibility(View.VISIBLE);
        wholeCard.setAnimation(AnimationUtils.loadAnimation(WalletActivity.this, R.anim.slider_in_bottom));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                airtimeCard.setVisibility(View.VISIBLE);
                airtimeCard.setAnimation(AnimationUtils.loadAnimation(WalletActivity.this, android.R.anim.fade_in));

                bankCard.setVisibility(View.VISIBLE);
                bankCard.setAnimation(AnimationUtils.loadAnimation(WalletActivity.this, android.R.anim.fade_in));

                cashCard.setVisibility(View.VISIBLE);
                cashCard.setAnimation(AnimationUtils.loadAnimation(WalletActivity.this, android.R.anim.fade_in));

                electricCard.setVisibility(View.VISIBLE);
                electricCard.setAnimation(AnimationUtils.loadAnimation(WalletActivity.this, android.R.anim.fade_in));

                tvCard.setVisibility(View.VISIBLE);
                tvCard.setAnimation(AnimationUtils.loadAnimation(WalletActivity.this, android.R.anim.fade_in));

                moreCard.setVisibility(View.VISIBLE);
                moreCard.setAnimation(AnimationUtils.loadAnimation(WalletActivity.this, android.R.anim.fade_in));
            }
        }, 750);

        findViewById(R.id.mobile_tpp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(WalletActivity.this, AirtimePurchaseActivity.class));
                caller("*242*6"+Uri.encode("#"));
            }
        });


        findViewById(R.id.bank_deposit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             useInternet();
            }
        });

        findViewById(R.id.cash_withdraw_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            caller(getCashUSSD());
            }
        });

        findViewById(R.id.payForElecticity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooserElectricity();
            }
        });

        findViewById(R.id.payForTv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            chooserTV();
            }
        });
        findViewById(R.id.payMoreBills).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                caller("*242*5"+Uri.encode("#"));
            }
        });


    }

    void showAccountBalance() {

        Observable<String> balanceObs = Observable.create(subscriber -> {
            OfflineDb offlineDb = new OfflineDb(WalletActivity.this);
            String referenceNumber = generateRandomReferenceNumber(offlineDb.getFirstname());
            String customerPrincipal = sortPhoneNumberExtension(offlineDb.getPhone());
            String customerCredentials = "Farmcrowdy"+offlineDb.getPin();
            //String destinationPhoneNumber = sortPhoneNumberExtension(offlineDb.getPhone());

            String sBuilder = referenceNumber+ ApplicationInstance.hashKey;
            Log.d("HASH", "has is : "+ sBuilder);


            final String hmac = hashSHA512(sBuilder).toString();
            Log.d("HASH", "has is : "+ hmac);
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("principal", ApplicationInstance.pagaPrincipal)
                            .addHeader("credentials",ApplicationInstance.pagaCredentials)
                            .addHeader("hash", hmac);

                    Request request = requestBuilder.build();

                    return chain.proceed(request);
                }
            });

            OkHttpClient client = httpClient.build();
            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl(ApplicationInstance.pagaBaseUrl)
                    .build();



            MyEndpoint myEndpoint = retrofit.create(MyEndpoint.class);
            // Call<ListOfBanksPojo>  caller = myEndpoint.buyAirtimePaga(new PagaBuyAirtime(referenceNumber, "en"));
            Call<PagaAccountBalanceResponse> caller = myEndpoint.getAccountBalance(new PagaAccountBalanceBody(referenceNumber,customerPrincipal,customerCredentials,"en"));
            caller.enqueue(new Callback<PagaAccountBalanceResponse>() {
                @Override
                public void onResponse(Call<PagaAccountBalanceResponse> call, Response<PagaAccountBalanceResponse> response) {
                    Log.e("Pagatech", "error exception due to inner "+response.code());
                    if(response.isSuccessful()) {

                        if(response.body().getResponseCode() == 0) {
                            subscriber.onNext(String.valueOf(response.body().getAvailableBalance()));
                            subscriber.onCompleted();
                        }
                        else {
                            subscriber.onNext(response.body().getMessage());
                            subscriber.onCompleted();
                        }
                    }

                    else {
                        subscriber.onNext(response.message());
                        subscriber.onCompleted();
                    }
                    //withPhotoInterface.failure("error due to: "+ response.code());
                }

                @Override
                public void onFailure(Call<PagaAccountBalanceResponse> call, Throwable t) {
                    Log.e("Pagatech", "error exception due to "+t.getMessage());
                    subscriber.onNext(t.getMessage());
                    subscriber.onCompleted();
                }
            });

        });

        balanceObs.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                Log.e("Pagatech", "error exception due to outer "+s);
                //balanceText.setText("...");
            }
        });



    }
    void seeBalanceUSSD() {
        caller("*242*1"+ Uri.encode("#"));
    }

    void chooser() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setIcon(R.drawable.tfs_logo_);
        builderSingle.setTitle("Select method:-");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("offline: USSD");
        arrayAdapter.add("online: DATA");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: useUSSD();
                        break;
                    case 1: useInternet();
                        break;
                }
            }
        });
        builderSingle.show();
    }

    void useUSSD() {

    }
    void useInternet() {
        startActivity(new Intent(this, BankDepostActivity.class));
    }

    public static Object hashSHA512(String message) {

        try {
            String toReturn = null;
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-512");
                digest.reset();
                digest.update(message.getBytes("utf8"));
                toReturn = String.format("%040x", new BigInteger(1, digest.digest()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return toReturn;

        } catch(Exception e) {
            return null;
        }
    }

    String sortPhoneNumberExtension(String initialNumber) {
      //  initialNumber = "08137102286";
        String finalString = "";
        if(initialNumber.substring(0,1).equals("0") ) {
            finalString = "+234"+initialNumber.substring(1);
        }
        return  finalString;
    }
    String generateRandomReferenceNumber(String firstname) {
        String initial = "Farmcrowdy";
        int middle = new Random().nextInt(9999);
        String lastOne = firstname.substring(2);
        long timing = new Date().getTime();
        return String.valueOf(initial+middle+""+timing);
    }


    void caller(String sts) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + sts));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(WalletActivity.this, new String[]{Manifest.permission.CALL_PHONE},407);
                return;

            }
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("Calling a Phone Number", "Call failed", activityException);
        }
    }
    String getCashUSSD() {
        return "*242*3"+ Uri.encode("#");
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 407: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Intent callIntent = new Intent(Intent.ACTION_CALL);
                    //callIntent.setData(Uri.parse("tel:" + getUSSD()));
                    //startActivity(callIntent);
                }
                else
                {

                }
                return;
            }
        }
    }

    void chooserElectricity() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setIcon(R.drawable.tfs_logo_);
        builderSingle.setTitle("Select Distribution Company:-");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Jos Electricity (JED)");
        arrayAdapter.add("Port Harcourt Electricity (PHED)");
        arrayAdapter.add("Kano Electricity (KEDCO)");
        arrayAdapter.add("Abuja Electricity (AEDC)");
        arrayAdapter.add("Eko Electricity (EKEDC)");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: caller("*242*817"+Uri.encode("#"));
                        break;
                    case 1: caller("*242*816"+Uri.encode("#"));
                        break;
                    case 2: caller("*242*819"+Uri.encode("#"));
                        break;
                    case 3: caller("*242*803"+Uri.encode("#"));
                        break;
                    case 4: caller("*242*818"+Uri.encode("#"));
                        break;
                }
            }
        });
        builderSingle.show();
    }

    void chooserTV() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setIcon(R.drawable.tfs_logo_);
        builderSingle.setTitle("Select TV providers:-");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("DStv");
        arrayAdapter.add("GOtv");
        arrayAdapter.add("StarTimes");
        arrayAdapter.add("Consat");
        arrayAdapter.add("Montage");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: caller("*242*810"+Uri.encode("#"));
                        break;
                    case 1: caller("*242*820"+Uri.encode("#"));
                        break;
                    case 2: caller("*242*813"+Uri.encode("#"));
                        break;
                    case 3: caller("*242*804"+Uri.encode("#"));
                        break;
                    case 4: caller("*242*811"+Uri.encode("#"));
                        break;
                }
            }
        });
        builderSingle.show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
