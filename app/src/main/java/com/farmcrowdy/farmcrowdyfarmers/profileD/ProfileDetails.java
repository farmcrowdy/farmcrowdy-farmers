package com.farmcrowdy.farmcrowdyfarmers.profileD;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdyfarmers.ApplicationInstance;
import com.farmcrowdy.farmcrowdyfarmers.MyEndpoint;
import com.farmcrowdy.farmcrowdyfarmers.R;
import com.farmcrowdy.farmcrowdyfarmers.engines.FarmerDetailPojo;
import com.farmcrowdy.farmcrowdyfarmers.engines.FarmerDetailsObject;
import com.farmcrowdy.farmcrowdyfarmers.engines.TinyDB;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileDetails extends AppCompatActivity {

    TextView genderId, dobId, groupId, locationId, cropId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_details_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        TextView farmerName = findViewById(R.id.myName);
        farmerName.setText(new TinyDB(this).getString("name"));

        TextView phone = findViewById(R.id.myPhone);
        String phoneString = new TinyDB(this).getString("farmer_phone");
        if(!phoneString.substring(0,1).equals("0")) phone.setText("0"+new TinyDB(this).getString("farmer_phone"));
        else phone.setText(phoneString);


        genderId = findViewById(R.id.genderId);
        dobId = findViewById(R.id.dobId);
        groupId = findViewById(R.id.groupId);
        locationId = findViewById(R.id.locationID);
        cropId = findViewById(R.id.cropId);


        checkDetailsOnline();

    }

    void checkDetailsOnline() {
        Log.d("DIMPLES", "First Done by: "+ new TinyDB(this).getInt("login", 2));
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofitTfs().create(MyEndpoint.class);
        Call<FarmerDetailsObject>  caller = myEndpoint.getFarmerShortInfo(new TinyDB(this).getInt("login", 2));
        caller.enqueue(new Callback<FarmerDetailsObject>() {
            @Override
            public void onResponse(Call<FarmerDetailsObject> call, Response<FarmerDetailsObject> response) {
                if(response.isSuccessful()) {
                    ImageView imageView = findViewById(R.id.myPhoto);
                    String init = "https://www.farmcrowdy.com/farmapp/assets/images/farmerpix/";
                    Picasso.with(ProfileDetails.this).load(init+response.body().getMessage().getPro_image()).placeholder(R.drawable.tfs_logo_).into(imageView);
                    genderId.setText(response.body().getMessage().getGender());
                    dobId.setText(response.body().getMessage().getDob());
                    groupId.setText(response.body().getMessage().getGroup_name());
                    locationId.setText(response.body().getMessage().getLocation());
                    cropId.setText(response.body().getMessage().getGr_crop_type());

                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(ProfileDetails.this, ViewPhotoActivity.class);
                            intent.putExtra("data", "https://www.farmcrowdy.com/farmapp/assets/images/farmerpix/" + response.body().getMessage().getPro_image());
                            startActivity(intent);
                        }
                    });
                }
                else {
                    Toast.makeText(ProfileDetails.this, "Something went wrong while trying to fetch your details", Toast.LENGTH_LONG).show();
                    genderId.setText("Error, no internet connection");
                    dobId.setText("Error, no internet connection");
                    groupId.setText("Error, no internet connection");
                    locationId.setText("Error, no internet connection");
                    cropId.setText("Error, no internet connection");
                    Log.d("DIMPLES", "Done by: "+response.message());
                }
            }

            @Override
            public void onFailure(Call<FarmerDetailsObject> call, Throwable t) {
                Toast.makeText(ProfileDetails.this, "No internet connection, please turn on data and try again...", Toast.LENGTH_LONG).show();
                genderId.setText("Error, no internet connection");
                dobId.setText("Error, no internet connection");
                groupId.setText("Error, no internet connection");
                locationId.setText("Error, no internet connection");
                cropId.setText("Error, no internet connection");
                Log.d("DIMPLES", "Done by two: "+t.getMessage());
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
