package com.farmcrowdy.farmcrowdyfarmers.engines;

public class FarmerDetailPojo {
    String gender,dob,location,group_name,gr_crop_type,pro_image;

    public String getGender() {
        return gender;
    }

    public String getDob() {
        return dob;
    }

    public String getLocation() {
        return location;
    }

    public String getGroup_name() {
        return group_name;
    }

    public String getGr_crop_type() {
        return gr_crop_type;
    }

    public String getPro_image() {
        return pro_image;
    }
}
