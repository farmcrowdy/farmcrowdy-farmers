package com.farmcrowdy.farmcrowdyfarmers.engines;

import android.content.Context;

public class OfflineDb {
    private  Context context;
    public OfflineDb(Context context) {
        this.context = context;
    }

    public void saveWhileAtSuccessfulLogin(Context contexter, String phone, String pin, int farmerId, String firstname, String lastname){
        TinyDB tinyDB = new TinyDB(contexter);
        tinyDB.putString("phone", phone);
        tinyDB.putString("pin", pin);
        tinyDB.putInt("farmerId", farmerId);
        tinyDB.putString("firstname", firstname);
        tinyDB.putString("lastname", lastname);
    }

    public String getPhone() {
       // return new TinyDB(context).getString("phone");
        return "09076890445";
    }
    public String getPin() {
        //return new TinyDB(context).getString("pin");
        return "5831";
    }
    public int getFarmerId() {
        return new TinyDB(context).getInt("farmerId", 1);
    }
    public String getFirstname() {
        return new TinyDB(context).getString("firstname");
    }
    public String getLastname() {
        return  new TinyDB(context).getString("lastname");
    }
}
