package com.farmcrowdy.farmcrowdyfarmers;

import android.app.Application;
import android.content.Context;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ApplicationInstance extends Application {
    private static Retrofit retrofit;
    private static final String APPID= "cef8cc3ce7f9421c0d9438a2c86154a0";
    private  static Context context;
    private static Retrofit retrofitTfs;
    public static String pagaPrincipal = "98F32858-CC3B-42D4-95A3-742110A8D405";
    public static String pagaCredentials = "rR9@f8u@bBES";
    public static String hashKey = "d98076e2d14c4045970edc466faa2ec8cc47c9b89b654001b5e4db27179a0b9559bee92b78034c558a9d24aca2fa4135db8938a3f4a74b7da1157dee68e15213";
    public static String pagaBaseUrl = "https://mypaga.com/";

    @Override
    public void onCreate() {
        super.onCreate();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        context = this;
        FirebaseAnalytics.getInstance(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/product_sans_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();


                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder();
                       // .header("api-key", "aa47f8215c6f30a0dcdb2a36a9f4168e"); // <-- this is the important line
                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

                retrofit = new Retrofit.Builder()
                       // .client(client)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://api.openweathermap.org/")
                .build();



        OkHttpClient.Builder httpClientTfs = new OkHttpClient.Builder();
        httpClientTfs.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("api-key", "aa47f8215c6f30a0dcdb2a36a9f4168e"); // <-- this is the important line
                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });
        OkHttpClient clientTfs = httpClientTfs.build();
        retrofitTfs = new Retrofit.Builder()
                .client(clientTfs)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://farmcrowdy.com/farmapp/api/")
                .build();
    }

    public static Retrofit getRetrofitTfs() {
        return retrofitTfs;
    }

    public static Context getContext() {
        return context;
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    public static String getAPPID() {
        return APPID;
    }
}
