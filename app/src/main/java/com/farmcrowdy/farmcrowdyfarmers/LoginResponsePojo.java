package com.farmcrowdy.farmcrowdyfarmers;

public class LoginResponsePojo {
    String farmername,lastname,phone, tfsphone, tfsname;
            int farmerid, tfsid;

    public String getFarmername() {
        return farmername;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPhone() {
        return phone;
    }

    public String getTfsphone() {
        return tfsphone;
    }

    public String getTfsname() {
        return tfsname;
    }

    public int getFarmerid() {
        return farmerid;
    }

    public int getTfsid() {
        return tfsid;
    }
}
