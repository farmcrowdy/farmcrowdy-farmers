package com.farmcrowdy.farmcrowdyfarmers.weatherActivity;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.bumptech.glide.Glide;
import com.farmcrowdy.farmcrowdyfarmers.R;
import com.farmcrowdy.farmcrowdyfarmers.androidMavenWeatherSdk.WeatherPojoList;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {
    private WeatherPojoList weatherPojoList;
    private Context context;

    public WeatherAdapter(WeatherPojoList weatherPojoList, Context context) {
        this.weatherPojoList = weatherPojoList;
        this.context = context;
    }

    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.weather_list_items, viewGroup, false);
        return new WeatherViewHolder(view);
    }

    int checkThroughImages(String message) {
        int sf;
        switch (message){
            case "light rain":
                sf =  R.drawable.lightest_rain;
                break;
            case "broken clouds": sf = R.drawable.broken_clouds;
                break;
            case "moderate rain": sf = R.drawable.moderate_rain;
                break;
            case "heavy intensity rain": sf = R.drawable.heavy_rain;
                break;
            default: sf =0;
                break;
        }
        return  sf;

    }


    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder weatherViewHolder, int i) {


            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try {
                Date d = f.parse(weatherPojoList.getList().get(i).getDt_text());
                long milliseconds = d.getTime();
                weatherViewHolder.relativeTimeTextView.setReferenceTime(milliseconds);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        String upperString = weatherPojoList.getList().get(i).getWeather().get(0).getDescription().substring(0,1).toUpperCase() + weatherPojoList.getList().get(i).getWeather().get(0).getDescription().substring(1);
            weatherViewHolder.description.setText(upperString);






        if (weatherViewHolder.getAdapterPosition() == i) {
            int doner = checkThroughImages(weatherPojoList.getList().get(i).getWeather().get(0).getDescription());
            switch (doner) {
                case 0:
                    Glide.with(context)
                            .load("https://openweathermap.org/img/w/"+weatherPojoList.getList().get(i).getWeather().get(0).getIcon()+".png")
                            .into(weatherViewHolder.imageView);
                    break;
                default: Glide.with(context)
                        .load(doner)
                        .into(weatherViewHolder.imageView);
                    break;
            }
        } else {
            Glide.with(context).clear(weatherViewHolder.imageView);
            //weatherViewHolder.picture.setImageResource(R.drawable.ic_image_dark_grey_24dp);
        }

    }

    @Override
    public int getItemCount() {
        if(weatherPojoList.getList().size() > 12) return  12;
       else  return weatherPojoList.getList().size();
    }

    public class WeatherViewHolder extends RecyclerView.ViewHolder {
        RelativeTimeTextView relativeTimeTextView;
        TextView description;
        ImageView imageView;
        public WeatherViewHolder(@NonNull View itemView) {
            super(itemView);
            relativeTimeTextView = itemView.findViewById(R.id.inTime);
            description = itemView.findViewById(R.id.inDesc);
            imageView = itemView.findViewById(R.id.inImage);
        }
    }
}
