//package com.farmcrowdy.farmcrowdyfarmers.weatherActivity;
//
//import android.Manifest;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.pm.PackageManager;
//import android.graphics.Color;
//import android.location.Location;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.View;
//import android.view.animation.AnimationUtils;
//import android.widget.ImageView;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.bumptech.glide.Glide;
//import com.farmcrowdy.farmcrowdyfarmers.ApplicationInstance;
//import com.farmcrowdy.farmcrowdyfarmers.MyEndpoint;
//import com.farmcrowdy.farmcrowdyfarmers.R;
//import com.farmcrowdy.farmcrowdyfarmers.androidMavenWeatherSdk.WeatherPojoList;
//
//import im.delight.android.location.SimpleLocation;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//import com.google.android.gms.location.LocationRequest;
//import com.vishalsojitra.easylocation.EasyLocationAppCompatActivity;
//import com.vishalsojitra.easylocation.EasyLocationRequest;
//import com.vishalsojitra.easylocation.EasyLocationRequestBuilder;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
//
//public class WeatherActivityClass extends EasyLocationAppCompatActivity {
//    RecyclerView recyclerView;
//    ProgressDialog progressDialog;
//    SimpleLocation location;
//    ImageView weatherIcon;
//    TextView weatherDesc;
//    ProgressBar ppr;
//    String initlat="0.00", initlong="0.00";
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.weather_activity_layout);
//
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        //toolbar.setTitleTextColor(Color.WHITE);
//        setSupportActionBar(toolbar);
//        toolbar.setTitleTextColor(Color.WHITE);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
//
//        ppr = findViewById(R.id.ppr);
//        weatherDesc = findViewById(R.id.weather_Desc);
//        weatherIcon = findViewById(R.id.currentWeather);
//
//        location = new SimpleLocation(this);
//        recyclerView = findViewById(R.id.weatherRecyclerViewId);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(linearLayoutManager);
//        progressDialog = new ProgressDialog(this);
//        recyclerView.setHasFixedSize(true);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setCancelable(false);
//        progressDialog.setMessage("Already fetching data from Open weather map!");
//
//        LocationRequest locationRequest = new LocationRequest()
//                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
//                .setInterval(5000)
//                .setFastestInterval(5000);
//        EasyLocationRequest easyLocationRequest = new EasyLocationRequestBuilder()
//                .setLocationRequest(locationRequest)
//                .setFallBackToLastLocationTime(3000)
//                .build();
//        requestSingleLocationFix(easyLocationRequest);
//
//    }
//
//    @Override
//    public void onLocationPermissionGranted() {
//        showToast("Location permission granted");
//    }
//
//    private void showToast(String message) {
//        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onLocationPermissionDenied() {
//        showToast("Location permission denied");
//    }
//    @Override
//    public void onLocationProviderEnabled() {
//        showToast("Location services are now ON");
//    }
//
//    @Override
//    public void onLocationProviderDisabled() {
//        showToast("Location services are still Off");
//    }
//
//    @Override
//    public void onLocationReceived(Location location) {
//        initlat = String.valueOf(location.getLatitude());
//        initlong = String.valueOf(location.getLongitude());
//         // showToast(location.getProvider() + "," + location.getLatitude() + "," + location.getLongitude());
//        findWeatherWithNeccesaryChecks();
//    }
//
//    void findWeatherWithNeccesaryChecks() {
//        if (!location.hasLocationEnabled()) {
//            // ask the user to enable location access
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setMessage("Kindly turn on your location at your settings");
//            builder.setTitle("Location settings");
//            builder.setCancelable(false);
//            builder.setPositiveButton("Open settings", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    SimpleLocation.openSettings(WeatherActivityClass.this);
//                }
//            });
//            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    finish();
//                }
//            });
//            builder.show();
//
//        }
//        else findLocation1();
//    }
//
//    void findLocation1() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
//                    == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
//                    == PackageManager.PERMISSION_GRANTED) {
//
//                        final double latitude = location.getLatitude();
//                        final double longitude = location.getLongitude();
//                        Log.d("OOK", "lat: "+initlat +" with longi:"+initlong);
//                        fetchWeather(Double.valueOf(initlat), Double.valueOf(initlong));
//
//            }
//            else {
//                ActivityCompat.requestPermissions(this,
//                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
//                        440);
//            }
//        }
//    }
//    private void fetchWeather(final double lat, final  double longi) {
//        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
//        Call<WeatherPojoList> weatherPojoListCall = myEndpoint.getWeatherForecastWithLatLong
//                (lat, longi,"json", ApplicationInstance.getAPPID());
//        weatherPojoListCall.enqueue(new Callback<WeatherPojoList>() {
//            @Override
//            public void onResponse(Call<WeatherPojoList> call, Response<WeatherPojoList> response) {
//                if(response.isSuccessful()) {
//                    showSuccess(response.body());
//                }
//                else showFailure();
//            }
//
//            @Override
//            public void onFailure(Call<WeatherPojoList> call, Throwable t) {
//                showFailure(t.getMessage());
//            }
//        });
//
//    }
//    void showSuccess(WeatherPojoList weatherPojoList) {
//        progressDialog.dismiss();
//        progressDialog.cancel();
//        weatherDesc.setText(String.valueOf("Now - "+weatherPojoList.getList().get(0).getWeather().get(0).getDescription()));
//
//        int doner = checkThroughImages(weatherPojoList.getList().get(0).getWeather().get(0).getDescription());
//        switch (doner) {
//            case 0:
//            Glide.with(this)
//                    .load("https://openweathermap.org/img/w/"+weatherPojoList.getList().get(0).getWeather().get(0).getIcon()+".png")
//                    .into(weatherIcon);
//                break;
//                default: Glide.with(this)
//                        .load(doner)
//                        .into(weatherIcon);
//                break;
//        }
//
//        ppr.setVisibility(View.GONE);
//        recyclerView.setAdapter(new WeatherAdapter(weatherPojoList, this));
//        recyclerView.setAnimation(AnimationUtils.loadAnimation(this, R.anim.abc_slide_in_bottom));
//        recyclerView.setVisibility(View.VISIBLE);
//        //Toast.makeText(this, "Successful!!", Toast.LENGTH_SHORT).show();
//    }
//    int checkThroughImages(String message) {
//        int sf;
//        switch (message){
//            case "light rain":
//               sf =  R.drawable.lightest_rain;
//               break;
//            case "broken clouds": sf = R.drawable.broken_clouds;
//                break;
//            case "moderate rain": sf = R.drawable.moderate_rain;
//            break;
//            case "heavy intensity rain": sf = R.drawable.heavy_rain;
//            break;
//            default: sf =0;
//            break;
//        }
//        return  sf;
//
//    }
//    void showFailure() {
//        progressDialog.dismiss();
//        progressDialog.cancel();
//        Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
//    }
//    void showFailure(String error) {
//        progressDialog.dismiss();
//        progressDialog.cancel();
//        Log.d("OOKS", "error due to "+error);
//        Toast.makeText(this, "Failed "+error, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case 440: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay! Do the
//                    // location-related task you need to do.
//                    if (ContextCompat.checkSelfPermission(this,
//                            Manifest.permission.ACCESS_FINE_LOCATION)
//                            == PackageManager.PERMISSION_GRANTED) {
//
//                        //Request location updates:
//                        //locationManager.requestLocationUpdates(provider, 400, 1, this);
//                        final double latitude = location.getLatitude();
//                        final double longitude = location.getLongitude();
//                        fetchWeather(latitude, longitude);
//
//                    }
//
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//
//                }
//                return;
//            }
//
//        }
//    }
//
//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }
//
//}
