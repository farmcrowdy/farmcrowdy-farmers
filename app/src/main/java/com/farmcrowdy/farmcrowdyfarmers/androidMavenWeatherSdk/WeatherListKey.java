package com.farmcrowdy.farmcrowdyfarmers.androidMavenWeatherSdk;

import java.util.ArrayList;

public class WeatherListKey {
WeatherMain main;
ArrayList<WeatherInfo> weather;
String dt_txt;

    public String getDt_text() {
        return dt_txt;
    }


    public WeatherMain getMain() {
        return main;
    }

    public ArrayList<WeatherInfo> getWeather() {
        return weather;
    }
}
