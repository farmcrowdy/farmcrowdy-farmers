package com.farmcrowdy.farmcrowdyfarmers.androidMavenWeatherSdk;

public class WeatherInfo {
    String main, description, icon;

    public String getIcon() {
        return icon;
    }

    public String getMain() {
        return main;
    }

    public String getDescription() {
        return description;
    }
}
