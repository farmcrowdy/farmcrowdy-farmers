package com.farmcrowdy.farmcrowdyfarmers.androidMavenWeatherSdk;

import java.util.ArrayList;

public class WeatherPojoList {
    int cod;
    ArrayList<WeatherListKey> list;

    public int getCod() {
        return cod;
    }

    public ArrayList<WeatherListKey> getList() {
        return list;
    }
}
