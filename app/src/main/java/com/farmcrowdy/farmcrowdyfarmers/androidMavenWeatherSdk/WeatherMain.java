package com.farmcrowdy.farmcrowdyfarmers.androidMavenWeatherSdk;

public class WeatherMain {
     float temp, temp_min, temp_max, pressure, sea_level, grnd_level, temp_kf;
     int  humidity;

    public float getTemp() {
        return temp;
    }

    public float getTemp_min() {
        return temp_min;
    }

    public float getTemp_max() {
        return temp_max;
    }

    public float getPressure() {
        return pressure;
    }

    public float getSea_level() {
        return sea_level;
    }

    public float getGrnd_level() {
        return grnd_level;
    }

    public float getTemp_kf() {
        return temp_kf;
    }

    public int getHumidity() {
        return humidity;
    }
}
