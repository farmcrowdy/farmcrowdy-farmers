package com.farmcrowdy.farmcrowdyfarmers;

import com.farmcrowdy.farmcrowdyfarmers.androidMavenWeatherSdk.WeatherPojoList;
import com.farmcrowdy.farmcrowdyfarmers.engines.FarmerDetailPojo;
import com.farmcrowdy.farmcrowdyfarmers.engines.FarmerDetailsObject;
import com.farmcrowdy.farmcrowdyfarmers.records.HarvestPojo;
import com.farmcrowdy.farmcrowdyfarmers.records.InputPojo;
import com.farmcrowdy.farmcrowdyfarmers.records.MonitoringPojo;
import com.farmcrowdy.farmcrowdyfarmers.wallets.ListOfBanksPojo;
import com.farmcrowdy.farmcrowdyfarmers.wallets.PagaAccountBalanceBody;
import com.farmcrowdy.farmcrowdyfarmers.wallets.PagaAccountBalanceResponse;
import com.farmcrowdy.farmcrowdyfarmers.wallets.PagaAirtimeBody;
import com.farmcrowdy.farmcrowdyfarmers.wallets.PagaBankListBody;
import com.farmcrowdy.farmcrowdyfarmers.wallets.PagaValidaateBody;
import com.farmcrowdy.farmcrowdyfarmers.wallets.ValidateBankPojo;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MyEndpoint {

    @Multipart
    @POST("farmerlogin")
    Call<LoginMainResponse> doFarmerLogin(@Part("pin") int Pin, @Part("phone")RequestBody phone);

    @GET("data/2.5/forecast")
    Call<WeatherPojoList> getWeatherForecastWithLatLong (@Query("lat") double latitude,
                                                         @Query("lon") double longitude,
                                                         @Query("mode") String mode,
                                                         @Query("APPID") String apiKey);


    @GET("farmers/{id}")
    Call<FarmerDetailsObject> getFarmerShortInfo(@Path("id") int id);
    @GET("inputByFarmer/{id}")
    Call<InputPojo> getInputs (@Path("id") int id);

    @GET("cropmonitorByFarmerid/{id}")
    Call<MonitoringPojo> getMonitoring (@Path("id") int id);

    @GET("harvestByFarmer/{id}")
    Call<HarvestPojo> getHarvests (@Path("id") int id);

    @Multipart
    @POST("message/{id}")
    Call<ResponseBody> sendMessageToTFS(
            @Path("id") int farmerDD,

            @Part("msg_tsf_id") int tfs_id,
            @Part("msg_farmer_id") int farmerId,
            @Part("message_text") RequestBody messageText,
            @Part("msg_videolink") RequestBody VideoLink,
            @Part("msg_photolink") RequestBody PhotoLink,
            @Part("msg_audiolink") RequestBody audioLink,
            @Part MultipartBody.Part msg_videolink,
            @Part MultipartBody.Part msg_photolink,
            @Part MultipartBody.Part msg_audiolink);

    @Multipart
    @POST("message/{id}")
    Call<ResponseBody> sendMessageToTFS(
            @Path("id") int farmerDD,
            @Part("msg_tsf_id") int tfs_id,
            @Part("msg_farmer_id") int farmerId,
            @Part("message_text") RequestBody messageText);


    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("paga-webservices/business-rest/secured/airtimePurchase")
    Call<PagaResponsePojo> buyAirtimeFromPaga(@Body PagaAirtimeBody pagaAirtimeBody);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("paga-webservices/business-rest/secured/getBanks")
    Call<ListOfBanksPojo> listOfBanksPaga(@Body PagaBankListBody pagaBankListBody);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("paga-webservices/business-rest/secured/validateDepositToBank")
    Call<ValidateBankPojo> validateAccount(@Body PagaValidaateBody pagaBankListBody);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("paga-webservices/business-rest/secured/withdraw")
    Call<PagaResponsePojo> finalDepositToAccount(@Body PagaValidaateBody pagaBankListBody);


    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("paga-webservices/business-rest/secured/accountBalance")
    Call<PagaAccountBalanceResponse> getAccountBalance(@Body PagaAccountBalanceBody accountBalanceBody);


}
