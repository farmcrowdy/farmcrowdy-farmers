package com.farmcrowdy.farmcrowdyfarmers;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdyfarmers.engines.OfflineDb;
import com.farmcrowdy.farmcrowdyfarmers.engines.TinyDB;
import com.farmcrowdy.farmcrowdyfarmers.messageTFS.MessageTfsActivity;
import com.farmcrowdy.farmcrowdyfarmers.profileD.ProfileDetails;
import com.farmcrowdy.farmcrowdyfarmers.records.RecordsActivity;
import com.farmcrowdy.farmcrowdyfarmers.wallets.WalletActivity;
//import com.farmcrowdy.farmcrowdyfarmers.weatherActivity.WeatherActivityClass;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MainActivity extends AppCompatActivity {
    private NavigationView mNavigationView;
    DrawerLayout mDrawerLayout;
    CardView wholer, recordsCard, weatherCard, superCard, walleyCard;
    private final static int REQUEST_PHONE_CALL = 209;
    String tfs_phone;
    TextView myName, myPhone;
    ImageView profileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(new TinyDB(this).getInt("login", 0) <1){
            startActivity(new Intent(this, LoginActivity.class));
            finishAffinity();

            String customerPrincipal = "+2348137102286";
            String customerCredentials = "Blonde77@1";

            return;
        }
        new OfflineDb(this).saveWhileAtSuccessfulLogin(this,"08137102286", "Blonde77@1",4,"firstname", "lastname");


        setContentView(R.layout.activity_main);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#293737"));

        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);





        tfs_phone = new TinyDB(this).getString("tfs_phone");
        final ActionBar ab = getSupportActionBar();



        ab.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        ab.setDisplayHomeAsUpEnabled(true);
        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        switch (menuItem.getItemId()){
                            case R.id.nav_input:
                                Intent intent = new Intent(MainActivity.this, RecordsActivity.class);
                                intent.putExtra("pos", 0);
                                startActivity(intent);
                                //startActivity(new Intent(MainActivity.this, FarmUpdateActvity.class));
                                break;
                            case R.id.nav_harvest:
                                Intent intent2 = new Intent(MainActivity.this, RecordsActivity.class);
                                intent2.putExtra("pos", 1);
                                startActivity(intent2);
                               // new TinyDB(MainActivity.this).clear();
                                //startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                break;
                            case R.id.nav_monitoring:
                                Intent intent3 = new Intent(MainActivity.this, RecordsActivity.class);
                                intent3.putExtra("pos", 2);
                                startActivity(intent3);
                                //startActivity(new Intent(MainActivity.this, InventoryActivity.class));
//                                break;
//                            case R.id.nav_forecast:
//                                Intent intent4 = new Intent(MainActivity.this, WeatherActivityClass.class);
//                                startActivity(intent4);
                                break;
                            case R.id.nav_supervisor:
                                startActivity(new Intent(MainActivity.this, MessageTfsActivity.class));
                                break;
                            case R.id.nav_wallet:
                                startActivity(new Intent(MainActivity.this, WalletActivity.class));
                                break;
                            case R.id.nav_logout:
                              new TinyDB(MainActivity.this).clear();
                              startActivity(new Intent(MainActivity.this, LoginActivity.class));
                              finishAffinity();
                                break;


                        }
                        return false;
                    }
                });

        wholer = findViewById(R.id.GridLayout1);
        recordsCard = findViewById(R.id.recordCard);
        weatherCard = findViewById(R.id.weatherCard);
        superCard = findViewById(R.id.supervisorCard);
        walleyCard = findViewById(R.id.walleyCard);

        wholer.setVisibility(View.VISIBLE);
        wholer.setAnimation(AnimationUtils.loadAnimation(MainActivity.this, R.anim.slider_in_bottom));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                recordsCard.setVisibility(View.VISIBLE);
                recordsCard.setAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));

                weatherCard.setVisibility(View.VISIBLE);
                weatherCard.setAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));

                superCard.setVisibility(View.VISIBLE);
                superCard.setAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));

                walleyCard.setVisibility(View.VISIBLE);
                walleyCard.setAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));

                CardView callerCard = findViewById(R.id.callerCard);
                callerCard.setVisibility(View.VISIBLE);
                callerCard.setAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));
                callerCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        caller();
                    }
                });

            }
        }, 750);
        doSomeClickings();
        
        
        
    }
    void doSomeClickings() {
        findViewById(R.id.input_tabber).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RecordsActivity.class);
                intent.putExtra("pos", 0);
                startActivity(intent);
            }
        });

//        findViewById(R.id.weather_tabber).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent2 = new Intent(MainActivity.this, WeatherActivityClass.class);
//                intent2.putExtra("pos", 1);
//                startActivity(intent2);
//            }
//        });

        findViewById(R.id.wallet_tabber).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(MainActivity.this, WalletActivity.class);
                intent3.putExtra("pos", 2);
                startActivity(intent3);
            }
        });

        findViewById(R.id.supervisor_tabber).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MessageTfsActivity.class));
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.inflateHeaderView(R.layout.nav_header_main);

        myName = headerView.findViewById(R.id.myName);
        myPhone = headerView.findViewById(R.id.myPhone);
        
        myName.setText(new TinyDB(this).getString("name"));
        String phoneString = new TinyDB(this).getString("farmer_phone");
        if(!phoneString.substring(0,1).equals("0")) myPhone.setText("0"+phoneString);
        else {
            myPhone.setText(phoneString);
        }

        headerView.findViewById(R.id.ppDem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ProfileDetails.class));
            }
        });



    }

    void caller() {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            if(!tfs_phone.substring(0,1).equals("0")) tfs_phone = "0"+tfs_phone;
            callIntent.setData(Uri.parse("tel:" + tfs_phone));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
                return;

            }
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("Calling a Phone Number", "Call failed", activityException);
        }
    }



    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + tfs_phone));
                    startActivity(callIntent);
                }
                else
                {

                }
                return;
            }
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                //  Toast.makeText(this, "Updates", Toast.LENGTH_SHORT).show();
                break;


        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            super.onBackPressed();
        }
    }

    protected boolean isNavDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START);
    }

    protected void closeNavDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
