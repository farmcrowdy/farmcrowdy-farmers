package com.farmcrowdy.farmcrowdyfarmers.records;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdyfarmers.ApplicationInstance;
import com.farmcrowdy.farmcrowdyfarmers.LoginActivity;
import com.farmcrowdy.farmcrowdyfarmers.MyEndpoint;
import com.farmcrowdy.farmcrowdyfarmers.R;
import com.farmcrowdy.farmcrowdyfarmers.engines.TinyDB;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HarvestFragment extends Fragment {
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView callback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.records_layout, container,false);
        recyclerView = view.findViewById(R.id.records_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        progressBar = view.findViewById(R.id.ppr);
        callback = view.findViewById(R.id.callback);

        findRequests();
        return view;
    }

    void findRequests() {
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofitTfs().create(MyEndpoint.class);
        Call<HarvestPojo> caller = myEndpoint.getHarvests( new TinyDB(getContext()).getInt("login", 0));
        caller.enqueue(new Callback<HarvestPojo>() {
            @Override
            public void onResponse(Call<HarvestPojo> call, Response<HarvestPojo> response) {
                if(response.isSuccessful()) {
                    if(response.body().getMessage().size() > 0) {
                        recyclerView.setAdapter(new HavestAdapter(response.body()));
                        progressBar.setVisibility(View.GONE);
                        callback.setVisibility(View.GONE);
                    }
                    else {
                        progressBar.setVisibility(View.GONE);
                        callback.setText("No harvest recorded yet");
                    }


                }
                else {
                    progressBar.setVisibility(View.GONE);
                    callback.setText("Something went wrong, please try again "+response.message());
                }
            }

            @Override
            public void onFailure(Call<HarvestPojo> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                callback.setText("Something went wrong due to "+t.getMessage() );
            }
        });
    }
}
