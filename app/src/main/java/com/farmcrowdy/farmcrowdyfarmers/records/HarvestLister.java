package com.farmcrowdy.farmcrowdyfarmers.records;

public class HarvestLister {
    String crop_type, quantity_unit, comment, date_collected;
  //  int quantity;

    public int getQuantity() {
        return 1;
    }

    public String getCrop_type() {
        return crop_type;
    }

    public String getQuantity_unit() {
        return quantity_unit;
    }

    public String getComment() {
        return comment;
    }

    public String getDate_collected() {
        return date_collected;
    }
}
