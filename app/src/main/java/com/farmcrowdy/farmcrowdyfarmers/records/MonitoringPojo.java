package com.farmcrowdy.farmcrowdyfarmers.records;

import java.util.ArrayList;

public class MonitoringPojo {
    String status;
   ArrayList<MonitoringLister> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<MonitoringLister> getMessage() {
        return message;
    }
}
