package com.farmcrowdy.farmcrowdyfarmers.records;

import java.util.ArrayList;

public class InputPojo {
   String status;
   ArrayList<InputsLister> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<InputsLister> getMessage() {
        return message;
    }
}
