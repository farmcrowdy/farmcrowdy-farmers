package com.farmcrowdy.farmcrowdyfarmers.records;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdyfarmers.R;

public class MonitoringAdapter extends RecyclerView.Adapter<MonitoringAdapter.MonitoringViewHolder> {
    MonitoringPojo monitoringPojo;

    public MonitoringAdapter(MonitoringPojo monitoringPojo) {
        this.monitoringPojo = monitoringPojo;
    }

    @NonNull
    @Override
    public MonitoringViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.records_items, viewGroup, false);
        return new MonitoringViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MonitoringViewHolder monitoringViewHolder, int i) {
        monitoringViewHolder.r1.setText(monitoringPojo.getMessage().get(i).getMon_comment());
        monitoringViewHolder.r2.setText(monitoringPojo.getMessage().get(i).getFarming_stage());
        monitoringViewHolder.r3.setText(monitoringPojo.getMessage().get(i).getMon_date());
    }

    @Override
    public int getItemCount() {
        if(monitoringPojo !=null) return monitoringPojo.getMessage().size();
        else return 0;
    }

    public class MonitoringViewHolder extends RecyclerView.ViewHolder{
        TextView r1, r2, r3, r4;
        public MonitoringViewHolder(@NonNull View itemView) {
            super(itemView);
            r1 = itemView.findViewById(R.id.r1);
            r2 =  itemView.findViewById(R.id.r2);
            r3 =  itemView.findViewById(R.id.r3);
            r4 =  itemView.findViewById(R.id.r4);

        }
    }

}
