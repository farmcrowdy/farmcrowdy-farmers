package com.farmcrowdy.farmcrowdyfarmers.records;

public class InputsLister {
    String planting_stage, date, input_name;
    int farmer_input_id, farmer_input_quantity, user_assign_id;

    public String getPlanting_stage() {
        return planting_stage;
    }

    public String getDate() {
        return date;
    }

    public String getInput_name() {
        return input_name;
    }

    public int getFarmer_input_id() {
        return farmer_input_id;
    }

    public int getFarmer_input_quantity() {
        return farmer_input_quantity;
    }

    public int getUser_assign_id() {
        return user_assign_id;
    }
}
