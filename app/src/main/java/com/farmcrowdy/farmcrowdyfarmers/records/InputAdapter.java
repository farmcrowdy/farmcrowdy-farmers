package com.farmcrowdy.farmcrowdyfarmers.records;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdyfarmers.R;

public class InputAdapter extends RecyclerView.Adapter<InputAdapter.InputViewHolder> {
    InputPojo inputPojo;

    public InputAdapter(InputPojo inputPojo) {
        this.inputPojo = inputPojo;
    }

    @NonNull
    @Override
    public InputViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.records_items, viewGroup, false);
        return new InputViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InputViewHolder inputViewHolder, int i) {
        inputViewHolder.r1.setText(String.valueOf(inputPojo.getMessage().get(i).getInput_name()));
        inputViewHolder.r2.setText(inputPojo.getMessage().get(i).getFarmer_input_quantity() + " units");
        inputViewHolder.r3.setText(inputPojo.getMessage().get(i).getPlanting_stage());
        inputViewHolder.r4.setText(inputPojo.getMessage().get(i).getDate());
    }

    @Override
    public int getItemCount() {
        if(inputPojo !=null) return inputPojo.getMessage().size();
        else return 0;
    }

    public class InputViewHolder extends RecyclerView.ViewHolder {
        TextView r1, r2, r3, r4;
        public InputViewHolder(@NonNull View itemView) {
            super(itemView);

            r1 = itemView.findViewById(R.id.r1);
            r2 =  itemView.findViewById(R.id.r2);
            r3 =  itemView.findViewById(R.id.r3);
            r4 =  itemView.findViewById(R.id.r4);

        }
    }
}
