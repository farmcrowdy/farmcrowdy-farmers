package com.farmcrowdy.farmcrowdyfarmers.records;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.farmcrowdy.farmcrowdyfarmers.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RecordsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.records_harvest);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        ViewPager viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(new RecordPager(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        int pos = getIntent().getExtras().getInt("pos", 0);
        viewPager.setCurrentItem(pos,true);



    }
    private class RecordPager extends FragmentPagerAdapter {

        public RecordPager(FragmentManager fm) {
            super(fm);
        }
        String[] tabTitles = {"Input", "Harvest", "Monitoring"};

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new Fragment();
            switch (i) {
                case 0: fragment = new InputFragment();
                break;
                case 1: fragment = new HarvestFragment();
                break;
                case 2: fragment = new MonitoringFragment();
                break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
