package com.farmcrowdy.farmcrowdyfarmers.records;

import java.util.ArrayList;

public class HarvestPojo {
    String status;
    ArrayList<HarvestLister> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<HarvestLister> getMessage() {
        return message;
    }
}
