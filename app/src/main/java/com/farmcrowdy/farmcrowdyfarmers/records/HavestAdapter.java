package com.farmcrowdy.farmcrowdyfarmers.records;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdyfarmers.R;

public class HavestAdapter extends RecyclerView.Adapter<HavestAdapter.HarvestViewHolder> {
    HarvestPojo harvestPojo;

    public HavestAdapter(HarvestPojo harvestPojo) {
        this.harvestPojo = harvestPojo;
    }

    @NonNull
    @Override
    public HarvestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.records_items, viewGroup,false);
        return new HarvestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HarvestViewHolder harvestViewHolder, int i) {
        harvestViewHolder.r1.setText(harvestPojo.getMessage().get(i).getCrop_type());
        harvestViewHolder.r2.setText(harvestPojo.getMessage().get(i).getComment());
        harvestViewHolder.r3.setText(String.valueOf(harvestPojo.getMessage().get(i).getQuantity() + " "+harvestPojo.getMessage().get(i).getQuantity_unit()));
        harvestViewHolder.r4.setText(harvestPojo.getMessage().get(i).getDate_collected());
    }

    @Override
    public int getItemCount() {
        if(harvestPojo.getMessage() !=null) return harvestPojo.getMessage().size();
        else return 0;
    }

    public class HarvestViewHolder extends RecyclerView.ViewHolder {
        TextView r1, r2, r3, r4;
        public HarvestViewHolder(@NonNull View itemView) {
            super(itemView);
            r1 = itemView.findViewById(R.id.r1);
            r2 =  itemView.findViewById(R.id.r2);
            r3 =  itemView.findViewById(R.id.r3);
            r4 =  itemView.findViewById(R.id.r4);

        }
    }
}
