package com.farmcrowdy.farmcrowdyfarmers.records;

public class MonitoringLister {

    String farming_stage, mon_comment, mon_date;

    public String getFarming_stage() {
        return farming_stage;
    }

    public String getMon_comment() {
        return mon_comment;
    }

    public String getMon_date() {
        return mon_date;
    }
}
