package com.farmcrowdy.farmcrowdyfarmers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.farmcrowdy.farmcrowdyfarmers.engines.TinyDB;
import com.rafakob.floatingedittext.FloatingEditText;

import org.xml.sax.helpers.LocatorImpl;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    FloatingEditText keyEdit, nameEdit;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        keyEdit = findViewById(R.id.key_editText);
        nameEdit = findViewById(R.id.firstname_editText);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Logging in");

        findViewById(R.id.login_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!TextUtils.isEmpty(keyEdit.getText()) && !TextUtils.isEmpty(nameEdit.getText()) ) {
                    progressDialog.show();
                    TinyDB tinyDB = new TinyDB(LoginActivity.this);
                    tinyDB.putInt("login",4532);
                    tinyDB.putString("name", "Oluwatobi Akinpelu");
                    tinyDB.putString("tfs_name", "Chris");
                    tinyDB.putInt("tfs_id", 25);
                    tinyDB.putString("tfs_phone", "07068020499");
                    tinyDB.putString("farmer_phone", "08158020499");

                    MyEndpoint myEndpoint = ApplicationInstance.getRetrofitTfs().create(MyEndpoint.class);
                    RequestBody messageBody = RequestBody.create(MediaType.parse("text/plain"), keyEdit.getText().toString());

                    Call<LoginMainResponse> caller = myEndpoint.doFarmerLogin(Integer.valueOf(nameEdit.getText()), messageBody);
                    caller.enqueue(new Callback<LoginMainResponse>() {
                        @Override
                        public void onResponse(Call<LoginMainResponse> call, Response<LoginMainResponse> response) {
                            progressDialog.dismiss();
                            progressDialog.cancel();
                            if(response.isSuccessful()) {
                                Log.d("Meter", "farmerId: "+response.body().getMessage().get(0).getFarmerid());
                                Log.d("Meter", "tfs_phone "+response.body().getMessage().get(0).getTfsphone());

                                TinyDB tinyDB = new TinyDB(LoginActivity.this);
                                tinyDB.putInt("login",response.body().getMessage().get(0).getFarmerid());
                                tinyDB.putString("name", response.body().getMessage().get(0).getFarmername() +" "+response.body().getMessage().get(0).getLastname());
                                tinyDB.putString("tfs_name", response.body().getMessage().get(0).getTfsname());
                                tinyDB.putInt("tfs_id", response.body().getMessage().get(0).getTfsid());
                                tinyDB.putString("tfs_phone", response.body().getMessage().get(0).getTfsphone());
                                tinyDB.putString("farmer_phone", response.body().getMessage().get(0).getPhone());

                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                finishAffinity();

                            }
                            else {
                                Snackbar.make(getCurrentFocus(), "Login credentials incorrect, please try again", Snackbar.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<LoginMainResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            progressDialog.cancel();
                            Log.d("LOGIN ISSUES", "ERROR IS: "+t.getMessage());
                            Snackbar.make(getCurrentFocus(), "No internet connection, please try again", Snackbar.LENGTH_SHORT).show();

                        }
                    });









                }
                else {
                    Snackbar.make(getCurrentFocus(), "Please enter both phone and PIN", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void doMainLogin() {

    }
}
